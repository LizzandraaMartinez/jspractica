import { Component } from '@angular/core';
import { Configuracion } from './models/configuracion';
import { from } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public title = 'Curso Javascript y Angular';
  public descripcion: string;
  public config;
  public mostrarVideojuegos:boolean = true;



  constructor(){
    this.config = Configuracion;
    this.title = Configuracion.titulo;
    this.descripcion = Configuracion.descripcion;
  }


  ocultarVideojuegos(value){
    this.mostrarVideojuegos = value;
  }
}
