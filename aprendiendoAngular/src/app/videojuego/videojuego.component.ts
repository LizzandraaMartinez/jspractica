import {
  Component, OnInit, DoCheck, OnDestroy
} from '@angular/core';

@Component({
      selector: 'videojuego',
      templateUrl: './videojuego.component.html'
})
export class VideojuegoComponent implements OnInit, DoCheck, OnDestroy{

    public titulo: string;
    public listado: string;

    constructor(){
        this.titulo = "Componente de videojuegos";
        this.listado = "Listado de los juegos más populares"

        //console.log("se ah cargado el componente");
    }
    ngOnInit(){
      //console.log("onInit Ejecutado");
    }
    ngDoCheck(){
      //console.log("DoCheck Ejecutado");
    }
    ngOnDestroy(){
      //console.log("OnDestroy Ejecutado");
    }
    cambiarTitulo(){
      this.titulo = "Nuevo título de componente";
    }
}