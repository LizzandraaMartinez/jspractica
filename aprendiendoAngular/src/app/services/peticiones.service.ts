import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class PeticionesService{

    public url: string;

    constructor(
        public _http: HttpClient
    ){
        this.url = "https://reqres.in/";
    }
}