import { Injectable } from '@angular/core';
import { Zapatilla } from '../models/zapatilla'

//Decorador
@Injectable()

export class ZapatillaService {

public zapatillas: Array <Zapatilla>;

  constructor() {

    this.zapatillas = [
      new Zapatilla("Nike Airmax", "Nike", "Rojo", 190, true),
      new Zapatilla("Reebok Classic", "Reebok", "Blanco", 80, true),
      new Zapatilla("Reebok Spartan", "Reebok", "Negro", 190, true),
      new Zapatilla("Nike Runner MD", "Nike", "Negra", 50, true),
      new Zapatilla("Addidas Yeezy", "Addidas", "Gris", 190, false)
    ];
  }

  getTexto(){
      return "Hola mundo desde un servicio";
  }

  getZapatillas():Array<Zapatilla>{
    return this.zapatillas;
  }
}
