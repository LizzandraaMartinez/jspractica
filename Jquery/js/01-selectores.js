$(document).ready(function () {
    //Selector ID
    var rojo = $("#rojo").css("background", "red").css("color", "white");

    var amarillo = $("#amarillo").css("background", "yellow").css("color", "green");

    var verde = $("#verde").css("background", "green").css("color", "white");


    //Selector de Clase
    var mi_clase = $(".zebra").css('padding', "5px");

    $(".sin_borde").click(function () {
        console.log("Click dado");
        $(this).addClass('zebra');
    })

    //Selectores de etiquetas
    var parrafos = $('p').css("cursor", "pointer");

    parrafos.click(function () {
        var this1 = $(this);

        if (!this1.hasClass('grande')) {
            this1.addClass('grande');
        } else {
            this1.removeClass('grande');
        }
    })


    //Selectores de Atributo
    $('[title="Google"]').css('background', '#ccc');
    $('[title="a"]').css('background', 'blue');


    //Otros
    //$('p, a').addClass('margenSuperior');

    var busqueda = $('#caja .resaltado').eq(0).parent().parent().find('[title="Google"]');
    console.log(busqueda);

})