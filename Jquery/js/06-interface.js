$(document).ready(function () {

    //Mover el elemento por la página
    $('.elemento').draggable();

    //Dedimensionar los elementos
    $('.elemento').resizable();

    //Seleccionar elementos
    //$('.listaSeleccionable').selectable();
    $('.listaSeleccionable').sortable({
        update: function(event, ui){
            console.log("La lista ha cambiado");
        }
    });

    //Drop
    $('#elementoMovido').draggable();
    $('#area').droppable({
        drop: function(){
            console.log("Has soltado algo dentro del área");
        }
    });

    //Efectos
    $('#mostrar').click(function(){
        $('#cajaEfectos').effect("explode");
    })

    //Tooltip
    $(document).tooltip();

    //Dialog
    $('#lanzarPopup').click(function(){
        $('.popup').dialog();
    })

    //Datepicker
    $('#calendario').datepicker();

    //Tabs
    $('#pestanas').tabs();
   
})