$(document).ready(function () {

    //MouseOver y MouseOut
    var caja = $('#caja');

    /*
    caja.mouseover(function(){
        $(this).css("background-color", "red");
    });


    caja.mouseout(function(){
        $(this).css("background-color", "green");
    })
    */

    //Evento Hover
    function cambiaRojo() {
        $(this).css("background-color", "red");
    }

    function cambiaVerde() {
        $(this).css("background-color", "green");
    }
    caja.hover(cambiaRojo, cambiaVerde);


    //Click, doble click
    caja.click(function () {
        $(this).css("background-color", "blue")
            .css("color", "white");
    })

    caja.dblclick(function () {
        $(this).css("background-color", "pink")
            .css("color", "yellow");
    })

    //Focus and blur

    var nombre = $('#nombre');
    $(nombre).focus(function () {
        $(this).css("border", "2px solid green");
    })
    $(nombre).blur(function () {
        $(this).css("border", "2px solid #ccc");
        $("#datos").text($(this).val()).show();
    })
})