//Modo Estricto
'use strict'

//Condicional if
//Si A es igual a B entonces haz algo

var edad = 18;
var Nombre = "Lizzie"

/*  
    //OPERADORES RELACIONALES 
        Mayor:          >
        Menor:          <
        Mayor o igual:  >=
        Menor o igual:  <=
        Igual:          ==
        Distinto:       !=
*/

if(edad >= 18){
    //Es mayor de edad
    console.log(Nombre + " tiene: " +edad + " años, es mayor de edad")

    if( edad <= "33"){
        console.log("todavía eres milenial");
    } else if(edad >= "70"){
        console.log("eres anciano");
    }else{
        console.log("ya no eres milenial");
    }

} else{
    //Es menor de edad
    console.log(Nombre + " tiene: " +edad + " años, es menor de edad")
}
/*
    //OPERADORES LÓGICOS
    AND         (Y)  &&
    OR          (O)  ||
    NEGACIÓN    (!)  !
    DIFERENTE   (!=) !=
*/
console.log("");

//Negación
var año = 2008;
if (año != "2016"){
    console.log("el año no es 2016, realmente es "+ año);
}

//AND
if(año >= "2000" && año <= "2020" && año != "2018"){
    console.log("estamos en la era actual");
}else{
    console.log("Estamos en la era moderna");
}
console.log("");

//OR
if(año == 2008 || año >= 2018 && año == 2028){
    console.log("El año acaba en 8");
}else{
    console.log("AÑO NO REGISTRADO");
}