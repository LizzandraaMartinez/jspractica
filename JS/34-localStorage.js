//Modo Estricto
'use strict'

//Localstorage

//Comprobar si el localstorage está disponible
if(typeof(Storage) !== 'undefined'){
    console.log("Localstorage disponible");
}else{
    console.log("Incompatible con Localstorage")
}

//Guardar datos
localStorage.setItem("titulo", "Curso de Symphony");

//Recuperar Elemento
document.querySelector("#peliculas").innerHTML = localStorage.getItem("titulo");

//Guardar objetos
var usuario = {
    nombre: "Lizzie Martinez",
    email: "lizzandramtz@gmail.com",
    web: "www.google.com"
};

localStorage.setItem("usuario", JSON.stringify(usuario));

//Recuperar Objeto
var userjs = JSON.parse(localStorage.getItem("usuario"));

console.log(userjs);
document.querySelector("#peliculas").append(" " + userjs.web+"  " +userjs.nombre);