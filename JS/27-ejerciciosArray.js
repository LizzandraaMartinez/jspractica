'use-strict'

/*
    1.- Pida 6 números por pantalla y los meta a un array.
    2.- Mostrar el array entero (en todos sus elementos) en el cuerpo de la página y de la consola.
    3.- Ordenarlo y mostrarlo.
    4.- Invertir su orden y mostrarlo.
    5.- Mostrar cuantos elementos tiene el array.
    6.- Búsqueda de un valor introducido por el usuario, que nos diga si lo encuentra y su índice (Se valorará el uso de funciones)
*/

function mostrarArray(elementos, textoCustom = "") {
    //Mostrar en el cuerpo de la página
    document.write("</h1>Contenido del array"+textoCustom+"</h1>");
    document.write("<ul>")
    elementos.forEach((elemento, index) => {
        document.write("<li>" + elemento + "</li><br>");
    });
    document.write("/<ul>")
}

//Pedir 6 números
var numeros = [];

for (var i = 0; i <= 5; i++) {
    //numeros[i] = parseInt(prompt("Introduce un número", 0))
    numeros.push(parseInt(prompt("Introduce un número", 0)));
}

//Mostrar en el cuerpo de la página
mostrarArray(numeros);

//Mostrar el array en la consola
console.log(numeros);

//Ordenar y mostrar
numeros.sort();
mostrarArray(numeros, "ordenado");