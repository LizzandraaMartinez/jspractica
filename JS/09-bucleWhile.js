//Modo Estricto
'use strict'

//bucle While

var año = 2008; 

while(año != 1991){
    //Ejecutará
    console.log("estamos en el año " + año);

}


//DoWhile

var años = 30; 

do {
    alert("solo cuando sea diferente a 20");
    años--;
}while(años > 25){

}