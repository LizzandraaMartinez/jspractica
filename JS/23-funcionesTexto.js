//Modo Estricto
'use strict'

//Transformación de Textos de Cadenas
var numero = 444;
var texto1 = "Hola, bienvenido al curso curso de JS";
var texto2 = "Es un muy buen curso";

/*
//Así se convierte la variable a String
var dato = numero.toString();
    dato = texto1.toUpperCase();
    dato = texto2.toLowerCase();
console.log(dato);

//Calcular longitud de texto
var nombre = "";
console.log(nombre.length);

//Concatenar (unir textos)
//var textoTotal = texto1 + texto2;
var textoTotal = texto1.concat("" + texto2);
console.log(textoTotal);

var busqueda = texto1.includes("JS");
console.log(busqueda);

//Reemplaza el texto
var busqueda = texto1.replace("JS", "CSS");
console.log(busqueda);
*/

//Separa el string
var busqueda = texto1.replace("JS", "CSS");
console.log(busqueda);