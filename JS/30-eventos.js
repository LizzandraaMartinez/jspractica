//Modo Estricto
'use strict'

//Eventos del Ratón

window.addEventListener('load', () => {
    function cambiarColor() {
        console.log("Me has dado click");

        var bg = boton.style.background;

        if (bg == "green") {
            boton.style.background = "red"

        } else {
            boton.style.background = "green"
        }
        boton.style.padding = "10px"
        boton.style.border = "1px solid #ccc"
        return true;
    }

    //Evento Click
    boton.addEventListener('click', function () {
        cambiarColor()
    });

    //Mouse Over
    boton.addEventListener('mouseover', function () {
        boton.style.background = "#ccc"
    })

    //Focus
    var input = document.querySelector("#campoNombre");

    input.addEventListener('focus', function () {
        console.log("[focus] Estás dentro del input");
    })

    //Blur
    input.addEventListener('blur', function () {
        console.log("[blur] Estás fuera del input");
    })

    //Keydown
    input.addEventListener('keydown', function (event) {
        console.log("Pulsando esta tecla", String.fromCharCode(event.keyCode));
    })

    //Keypress
    input.addEventListener('keypress', function (event) {
        console.log("Tecla presionada", String.fromCharCode(event.keyCode));
    })

    //Keyups
    input.addEventListener('keyup', function (event) {
        console.log("Tecla soltada", String.fromCharCode(event.keyCode));
    })
})