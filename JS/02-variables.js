//Modo Estricto
'use strict'

//Variables

//Una variable es un contenedor de información,
//Se asigna un valor a la variable
var pais = "España";
var continente = "Europa";
var antiguedad = 2019;
//Concatenación     
var paisyContinente = pais+ " " +continente;

//Let Permite definir variables con limitaciones
//Var define una variable para luego utilizarla
let prueba = "hola";
alert("hola");

//Para asignar un nuevo valor a una variable
pais = "México";
continente = "Latinoamérica";

console.log(pais,continente, antiguedad);
alert(paisyContinente);