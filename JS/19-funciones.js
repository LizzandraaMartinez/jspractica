'use-strict'

//Funciones
//Una función es una agrupación reutilizable de un conjunto de instrucciones

//Defino la función
function calculadora(){
    //Conjunto de instrucciones
    console.log("Hola soy la Calculadora");
    console.log("Si, soy yo")
    //return "Hola, soy una calculadora"; //la función devuelve algo
}
//Invocar o llamar a la función
calculadora();