//Tipos de Datos
//String
var cadena = 'Hola';
cadena = 12;
//Number
var numero = '12';
//Boolean
var verdaderoFalso = 'true';
//Any
var cualquierValor = 'hola';
//Arrays
var lenguajes = ["PHP", "JS", "CSS"];
var years = ["hola", 13, 14, 15];
//Let vs Var
var numero1 = 10;
var numero2 = 20;
if (numero1 === 10) {
    var numero1_1 = 44;
    var numero2_1 = 55;
    console.log(numero1_1, numero2_1);
}
console.log(numero1, numero2);
console.log(cadena, numero, verdaderoFalso, cualquierValor, lenguajes, years);
