//Tipos de Datos

type letrasNumeros = string | number;

    //String
    let cadena: letrasNumeros = 'Hola';

    cadena = 12;

    //Number
    let numero: number = '12';

    //Boolean
    let verdaderoFalso: boolean = 'true'

    //Any
    let cualquierValor: any = 'hola'

    //Arrays
    var lenguajes: Array<string> = ["PHP", "JS", "CSS"];

    let years: any[] = ["hola", 13, 14, 15];

    //Let vs Var
    var numero1 = 10; 
    var numero2 = 20; 

    if(numero1 === 10){
        let numero1 = 44;
        let numero2 = 55;

        console.log(numero1, numero2);
    }
    console.log(numero1, numero2);

    console.log(cadena, numero, verdaderoFalso, cualquierValor, lenguajes, years);