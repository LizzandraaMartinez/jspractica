//Clase(molde de objeto)
class Camiseta {

    //Propiedades (caracterísiticas del objeto)
    private color: string;
    private modelo: string;
    private marca: string;
    private talla: string;
    private precio: number;

    //Métodos (funciones o acciones del objeto)

    constructor(color, modelo, marca, talla, precio){
        this.color = color;
        this.modelo = modelo;
        this.marca = marca;
        this.talla = talla;
        this.precio = precio;
    }

    public setColor(color) {
        this.color = color;
    }
    public getColor() {
        return this.color;
    }
}

var camiseta = new Camiseta("rojo", "manga larga", "Nike", "L", 20);
camiseta.setColor("rojo");
//camiseta.getColor();

console.log(camiseta.getColor());