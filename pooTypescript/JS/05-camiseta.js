//Clase(molde de objeto)
var Camiseta = /** @class */ (function () {
    //Métodos (funciones o acciones del objeto)
    function Camiseta(color, modelo, marca, talla, precio) {
        this.color = color;
        this.modelo = modelo;
        this.marca = marca;
        this.talla = talla;
        this.precio = precio;
    }
    Camiseta.prototype.setColor = function (color) {
        this.color = color;
    };
    Camiseta.prototype.getColor = function () {
        return this.color;
    };
    return Camiseta;
}());
var camiseta = new Camiseta("rojo", "manga larga", "Nike", "L", 20);
camiseta.setColor("rojo");
//camiseta.getColor();
console.log(camiseta.getColor());
