$(document).ready(function () {

    //SLIDER
    $('.bxslider').bxSlider({
        mode: 'fade',
        captions: true,
        slideWidth: 1200,
        responsive: true,
        pager: true
    });
    //SELECTOR DE TEMAS
    var theme = $("#theme");

    $('#toGreen').click(function(){
        theme.attr("href", "css/green.css")
    });

    $('#toBlue').click(function(){
        theme.attr("href", "css/blue.css")
    });

    $('#toRed').click(function(){
        theme.attr("href", "css/red.css")
    });

    //SCROLL ARRIBA DE LA WEB
    $('.subir').click(function(e){

        e.preventDefault();

        $('html, body').animate({
            scrollTop: 0,
        }, 500);
        
        return false;
    })
})